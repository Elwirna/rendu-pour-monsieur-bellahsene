const http = require("http");

const port = 8080;

http.createServer((request, response) => {

 request.on('error', (err) => {
  
    console.error(err);
    response.statusCode = 400;
    response.end();
  
  });
  
  response.on('error', (err) => {
  
    console.error(err);
  
  });

  if (request.method == 'POST' && request.url == '/echo') {
  
    request.pipe(response);
  
  } else {

    response.statusCode = 404;
    response.end();
  
  }


}).listen( port ); 