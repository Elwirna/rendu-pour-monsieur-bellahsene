const mqtt = require('mqtt');
const client = mqtt.connect('mqtt://localhost:8000');

var topic = 'user-test';
var message = 'Hello IOT world !';

client.on( 'connect', () =>{

	setInterval( () => {

		client.publish( topic, message );
		console.log( "Message sent =>", message);

	}, 5000);

});