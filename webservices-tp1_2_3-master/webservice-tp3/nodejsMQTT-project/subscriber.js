const mqtt = require('mqtt');
const client = mqtt.connect('mqtt://localhost:8000');

var topic = 'user-test';

client.on( 'connect', () => {
	
	client.subscribe( topic );

})

client.on( 'message', (topic,  message) => {

	console.log("Received => " + message.toString());

});

